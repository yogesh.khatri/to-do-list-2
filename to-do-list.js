// getting list and form reference in constant variables
const ulList = document.querySelector('#list');
const form = document.querySelector('#form');
const heading = document.querySelector('#heading');

const date = document.createElement('h2');
date.style.fontSize = '1rem';
date.innerText = new Date();
heading.appendChild(date);
// getting list items, in listItems array (string format to JSON object)
let listItems = JSON.parse(window.localStorage.getItem('listItems')) || [];

refreshDOM();
var counter = 0;

// definition of refreshDOM function
function refreshDOM() {
  // emptying the ul-list elements
  while (ulList.hasChildNodes()) {
    ulList.removeChild(ulList.childNodes[0]);
  }
  counter = 0;
  //  creating ul-list from local-storage
  for (let i = 0; i < listItems.length; i++) {
    creatingList(listItems[i]);
  }
}

//function for updating local storage (entering data in string format)
function updateListItems() {
  let checkedList = listItems.filter(listItem => listItem.checked == true);
  let uncheckedList = listItems.filter(listItem => listItem.checked == false);
  listItems = checkedList.concat(uncheckedList);
  localStorage.setItem('listItems', JSON.stringify(listItems));
  refreshDOM();
}

// create new list items
form.addEventListener('submit', function(e) {
  e.preventDefault();
  // getting text from input box on click on Add
  const inputText = form.querySelector('input[type="text"]').value;
  if (inputText.length > 0) {
    listItems.push({ toDo: inputText, checked: false });
    updateListItems();
  }
});

// creating List-items
function creatingList(listItem) {
  // creating checkbox  for list
  var checkbox = document.createElement('input');
  checkbox.type = 'checkbox';
  checkbox.className = 'checkbox';
  checkbox.checked = listItem['checked'];

  // creating paragraph tag with toDo text
  var paragraph = document.createElement('p');
  paragraph.className = 'paragraph-text';
  paragraph.textContent = listItem['toDo'];

  if (listItem.toDo.length > 50) {
    paragraph.style.height = '4rem';
  }

  // creating delete button for list
  var deleteBtn = document.createElement('button');
  deleteBtn.innerHTML = 'Delete';
  deleteBtn.className = 'deleteBtn';

  // creating new list-item
  const newListItem = document.createElement('li');
  newListItem.className = 'list-item';
  newListItem.setAttribute('position', counter++);
  newListItem.setAttribute('draggable', true);

  // append checkbox, paragraph & delete button to newListItem
  newListItem.appendChild(checkbox);
  if (listItems[counter - 1]['checked']) {
    paragraph.style.textDecoration = 'line-through';
    paragraph.style.color = 'grey';
    newListItem.appendChild(paragraph);
  } else {
    newListItem.appendChild(paragraph);
  }
  newListItem.appendChild(deleteBtn);

  // append newListItem at the beginning of list
  const firstElementOfList = document.querySelector('.list-item');
  ulList.insertBefore(newListItem, firstElementOfList);
  form.reset();
}

// delete and checkbox event handler
document.addEventListener('click', function(e) {
  const clicked = e.target.className;
  if (clicked == 'deleteBtn') {
    var position = e.target.parentElement.getAttribute('position');
    if (position > -1) {
      listItems.splice(position, 1);
    }
    updateListItems();
  }
  if (clicked == 'checkbox') {
    var position = e.target.parentElement.getAttribute('position');
    if (listItems[position]['checked']) {
      listItems[position]['checked'] = false;
      updateListItems();
    } else {
      listItems[position]['checked'] = true;
      updateListItems();
    }
  }
});

// drag functions
var dragStartPos;
var dragEndPos;
ulList.addEventListener('dragstart', dragStart);
ulList.addEventListener('dragover', dragOver);
ulList.addEventListener('dragend', dragEnd);

function dragStart(e) {
  dragStartPos = e.target.getAttribute('position');
}

function dragOver(e) {
  dragEndPos = e.target.getAttribute('position');
}

function dragEnd(e) {
  const tempNewList = listItems.splice(dragStartPos, 1);
  listItems.splice(dragEndPos, 0, tempNewList[0]);
  updateListItems();
}

console.log(
  'The concat method creates a new array consisting of the elements in the object'
    .length
);
